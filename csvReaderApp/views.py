# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import csv

from django.shortcuts import render
from django.conf import settings

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


class CsvReaderViewSet(APIView):
    """
    API endpoint that allows to get all data from csv.
    """
    def get(self, request):
        base_path = settings.BASE_DIR
        file_path = base_path + "/data.csv"
        with open(file_path, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=str(','), quotechar=str('|'))
            data = [{
            			'key': row[0],
            			'value': row[1]
            		} for row in reader]
        return Response(data, status=status.HTTP_200_OK)


class GetDataViewSet(APIView):
    """
    API endpoint that get selected key and value.
    """
    def get(self, request, key):
        if (key).isnumeric():
        	base_path = settings.BASE_DIR
        	file_path = base_path + "/data.csv"
        	with open(file_path, 'rb') as csvfile:
        		reader = csv.DictReader(csvfile, delimiter=str(','))
        		data = list(reader)
        		response = filter(lambda x: x.get('Value').lower() == str(key).lower(), data)
        	if len(response) > 0:
        		return Response(response, status=status.HTTP_200_OK)
        	else:
        		return Response({"error": "This value not present in csv file"}, status=status.HTTP_400_BAD_REQUEST)
        else:
        	return Response({"error": "Key must be numbers only"}, status=status.HTTP_400_BAD_REQUEST)
