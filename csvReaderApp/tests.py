# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, RequestFactory
from django.test.client import Client
from csvReaderApp.views import CsvReaderViewSet, GetDataViewSet

# Create your tests here.
class  CustomersTestCase(TestCase):
	def setUp(self):
		self.csvReader = CsvReaderViewSet()
		self.getData = GetDataViewSet()
		self.url_reserve1 = '/'
		self.url_reserve2 = '/'

		self.c = Client()
		self.factory = RequestFactory()

	def test_csv_data(self):
		request1 = self.factory.get(self.url_reserve1)
		create_reservation = self.csvReader.get(request1)
		self.assertTrue(len(create_reservation.data))


	def test_single_value(self):
		request1 = self.factory.get(self.url_reserve2)
		create_reservation = self.getData.get(request1, key=u'1')
		self.assertEqual('One', create_reservation.data[0]['Key'])


	def test_wrong_value(self):
		request1 = self.factory.get(self.url_reserve2)
		create_reservation = self.getData.get(request1, key=u'One')
		self.assertEqual('Key must be numbers only', create_reservation.data['error'])


	def test_non_existing_value_in_csv(self):
		request1 = self.factory.get(self.url_reserve2)
		create_reservation = self.getData.get(request1, key=u'1235')
		self.assertEqual('This value not present in csv file', create_reservation.data['error'])	
