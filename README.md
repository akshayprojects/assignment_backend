﻿

## Installation
To make a working build of assignment on your local system follow these instructions

#### Step 1
##### Installing Python
Download and install Python 2.7 on your system from [here](https://www.python.org/downloads/) depending on your Operating System and System Architecture.

#### Step 2
##### Making project folder
Create project folder in your workspace folder and make a folder for source code.
> The following commands are for Linux and Mac systems. For Windows do ask for someone's assistance.

```
cd workspace
mkdir assignment && cd assignment
mkdir src
```

#### Step 3
##### Getting the code on local system.

Let's get the code on your local system now.

```
cd src
git clone https://akshaykale@bitbucket.org/akshaykale/assignment_backend.git
```

#### Step 4 
##### Install Django/Virtualenv on a Mac OS X or Linux. 

When you see code like the following:


1. Open Terminal (Applications > Utilities > Terminal)
2. Enter the following commands:
NOTE: The command `sudo` will require an admin password.
	
	1. Install Pip. (Python Package Installer):

		```
		sudo easy_install pip
		```
	2. Install virtualenv:

		```
		sudo pip install virtualenv
		```

	4. Create a new virtualenv:

		```
		virtualenv yourenv -p python2.7
		``` 

		The name "yourenv" above is arbitrary. You can name it as you like.

	5. Activate virtualenv:

		```
		source bin/activate
		```
		The result in Terminal should be something like:
		```
		(yourenv) akshay-macbook-air:~ orion$
		``` 

	7. Install requirements:
	    ```
		  pip install -r requirements.txt
		```

To run the project find the manage.py in directory and in terminal type the following command.
```
python manage.py runserver
```

To run the test cases type the following command in terminal.
```
python manage.py test appname
```

Run using docker.

```
Visit (https://hub.docker.com/) and search for 'akshaykale1109/assignment_goibibo_web' 
```

Take a pull of image or run the docker run.
```
sudo docker pull akshaykale1109/assignment_goibibo_web
or
sudo docker run akshaykale1109/assignment_goibibo_web
```

To run the project
```
sudo docker-compose up
```

Api's
```
1) http://127.0.0.1:8000/
2) http://127.0.0.1:8000/1
3) http://127.0.0.1:8000/456
4) http://127.0.0.1:8000/One
```