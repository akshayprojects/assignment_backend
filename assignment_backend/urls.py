
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from csvReaderApp import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.CsvReaderViewSet.as_view()),
    url(r'^(?P<key>.+)$', views.GetDataViewSet.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
